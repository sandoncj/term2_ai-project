﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    public float sensitivity = 1;
    public bool canCameraRotate = true;
    private void Update()
    {
        if (canCameraRotate == true)
        {
            Rotation();
        }      
    }

    private void Rotation()
    {        
        //creates a float of the Y axis input.
        float xRot = Input.GetAxisRaw("Mouse Y");

        float eulerx = transform.eulerAngles.x;
        eulerx += xRot *-1;
        //sets the eulers angles transform of the camera to a new vector3
        //takes the Y axis float and assigns to the x rotation of the transform
        //euelerAngles.y is default (camera is a child of the player), eulerAngles.z is locked to 0 rotation
        transform.eulerAngles = new Vector3(eulerx, transform.eulerAngles.y, 0);
    }
}
