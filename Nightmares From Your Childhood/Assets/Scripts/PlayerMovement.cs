﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 1;
    public float sensitivity = 1;
    //canPlayerMove bool allows me to lock position and rotation of player at endGame.
    public bool canPlayerMove = true;

    private CharacterController player;

    private void Awake()
    {
        //gets the CharacterController componenet of the player object
        player = GetComponent<CharacterController>();
        Time.timeScale = 1;
    }

    private void Update()
    {       
        if (canPlayerMove == true)
        {
            Movement();
            Rotation();
        }
       
    }

    private void Movement()
    {
        //assigns a variable to vector3.zero
        //floats the vertical and horizontal axis movement to apply to the desitnation vector.
        Vector3 destination = Vector3.zero;
        float vert = Input.GetAxisRaw("Vertical");
        float hor = Input.GetAxisRaw("Horizontal");

        //applies the vert and hor axis inputs to the destination vector and multiplies by speed
        destination += transform.forward * vert * speed;
        destination += transform.right * hor * speed;
        //applies movement to the character controller using the destination vector and sets it to framrate independant
        player.Move(destination  * Time.deltaTime);
    }

    private void Rotation()
    {
        //floats the mouse X axis input.
        float yRot = Input.GetAxisRaw("Mouse X");
        //creates a vector3 to apply the transform
        //vector3.x and z are set to 0 to lock the rotation around the x and z axis
        Vector3 rotation = new Vector3(0, yRot, 0);
        //sets the eulerAngls transforms to the rotation vector
        player.transform.eulerAngles += rotation;
        
    }
}
