﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorLock : MonoBehaviour
{
    // creates a toggle for cursor lockmode. turning on gives more realistic game view for tests
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

}
