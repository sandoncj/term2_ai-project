﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    //public Enemy enemy;
    public GameObject enemy;
    public GameObject crosshair;
    public GameObject stunbar;

    public float stunCooldown = 5;
    public bool stunUsed = false;

    [SerializeField]
    private GameObject endScreen;
    [SerializeField]
    private GameObject endScreenCam;
    [SerializeField]
    private GameObject pause;
    [SerializeField]
    private GameObject welcome;
    [SerializeField]
    private GameObject toiletInteract;
    [SerializeField]
    private Sprite crosshairGreen;
    [SerializeField]
    private Sprite crosshairRed;

    private float timer;

    private void Update()
    {
        
        timer += Time.deltaTime;
        Interaction();
        PauseMenu();
        Stun();
        //if the welcom screen is active
        if (welcome.activeInHierarchy)
        {
            //execute the welcome screen function
            WelcomeScreen();
            //if the e key is pressed deactivate the welcome screen and allow the player and camera to move
            if (Input.GetKeyDown(KeyCode.E))
            {
                gameObject.transform.parent.gameObject.GetComponent<PlayerMovement>().canPlayerMove = true;
                gameObject.GetComponent<CameraMovement>().canCameraRotate = true;
                welcome.SetActive(false);
            }
        }
        if (toiletInteract.activeInHierarchy)
        {
            //execute the toiletinteraction screen function
            ToiletInteraction();        
        }
        if (stunCooldown == 5)
        {
            stunbar.GetComponent<Slider>().value = 1;
        }
        else
        {
            stunbar.GetComponent<Slider>().value = (0 + (5 - stunCooldown))/5;
        }
        
    }
    private void Interaction()
    {
        //creates a ray from the cameras position forwards
        //assigns raycasthit to variable hit
        Ray direction = new Ray(transform.position, transform.forward * 50);
        Debug.DrawRay(transform.position, transform.forward * 50, Color.blue);
        RaycastHit hit;

        crosshair.GetComponent<SpriteRenderer>().sprite = crosshairGreen;
        
        //checks if ray direction has hit a collider
        if (Physics.Raycast(direction, out hit))
        {
            //if the hit collider is not null (gets rid of null reference
            if (hit.collider != null)
            {
                //if the raycast hit the toilet and distance between the camera and toilet is less than 10
                if (hit.collider.tag.Equals("Toilet") && Vector3.Distance(transform.position, hit.collider.transform.position) < 10)
                {
                    crosshair.GetComponent<SpriteRenderer>().sprite = crosshairRed;
                    Debug.DrawRay(transform.position, transform.forward * 10, Color.green);
                    //toilet is the main objective
                    //if toilet is interacted with set the Ai alert to true and targetInRange to true
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        timer = 0;
                        toiletInteract.SetActive(true);
                        var ai = enemy.GetComponent<Enemy>();
                        ai.alert = true;
                        ai.targetInRange = true;
                    }
                }
                if (hit.collider.tag.Equals("Enemy") && Vector3.Distance(transform.position, hit.collider.transform.position) <= 30)
                {
                    crosshair.GetComponent<SpriteRenderer>().sprite = crosshairRed;
                    Debug.DrawRay(transform.position, transform.forward * 50, Color.red);
                    //if the raycast hit the enemy and player interacts with the enemy set the Ai state to stunned
                    if (stunCooldown ==5)
                    {
                        var ai = enemy.GetComponent<Enemy>();
                        if (Input.GetKeyDown(KeyCode.E))
                        {
                            //assigns a variable to the Enemy class component of the enemy gameobject                           
                            ai.isStunned = true;
                            stunUsed = true;
                            stunbar.GetComponent<Slider>().value = 0;
                        }

                        else if (Input.GetKeyDown(KeyCode.E) && ai.alert == true)
                        {
                            ai.isStunned = true;
                            stunUsed = true;
                            stunbar.GetComponent<Slider>().value = 0;
                        }
                    }
                   
                }
                //if the enemy is in the alert state and the player interacts with the bed (start position)
                if (enemy.GetComponent<Enemy>().alert == true)
                {
                    if (hit.collider.tag.Equals("Bed"))
                    {
                        crosshair.GetComponent<SpriteRenderer>().sprite = crosshairRed;
                        if (Input.GetKeyDown(KeyCode.E))
                        {
                            //sets timescale to 0 to lock Ai movement
                            //deactivates the player object
                            //sets the new cam to the endscreen cam and activates the endgame canvas
                            //sets the endWin bool in the enemy class to true to stop all audio         
                            Cursor.lockState = CursorLockMode.None;
                            Cursor.visible = true;
                            Time.timeScale = 0;
                            endScreenCam.SetActive(true);
                            gameObject.transform.parent.gameObject.SetActive(false);
                            enemy.GetComponent<Enemy>().gameStop = true;
                            endScreen.SetActive(true);
                        }
                    }                   
                }
            }
        }
    }

    private void Stun()
    {
        if (stunUsed == true)
        {
            stunCooldown -= Time.deltaTime;
        }
        if (stunCooldown <= 0)
        {
            stunUsed = false;
            stunCooldown = 5;
        }
        
    }

    private void PauseMenu()
    {
        //if the esc key is pressed and the pause menu not open
        if (Input.GetKeyDown(KeyCode.Escape) && pause.activeInHierarchy == false)
        {
            //unlocks the cursor
            //activates the pausemenu
            //sets the enemy gamestop variablke to true to stop audio
            //stops the player and camera moving
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            pause.SetActive(true);
            Time.timeScale = 0;
            enemy.GetComponent<Enemy>().gameStop = true;
            gameObject.transform.parent.gameObject.GetComponent<PlayerMovement>().canPlayerMove = false;
            gameObject.GetComponent<CameraMovement>().canCameraRotate = false;
        }
    }

    private void WelcomeScreen()
    {
        //locks the cursor
        //stops the player and camera from moving
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameObject.transform.parent.gameObject.GetComponent<PlayerMovement>().canPlayerMove = false;
        gameObject.GetComponent<CameraMovement>().canCameraRotate = false;
    }

    private void ToiletInteraction()
    {
        //locks the cursor
        //stops the player and camera from moving     
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameObject.transform.parent.gameObject.GetComponent<PlayerMovement>().canPlayerMove = false;
        gameObject.GetComponent<CameraMovement>().canCameraRotate = false;
        if (timer > 1.5)
        {
            gameObject.transform.parent.gameObject.GetComponent<PlayerMovement>().canPlayerMove = true;
            gameObject.GetComponent<CameraMovement>().canCameraRotate = true;
            toiletInteract.SetActive(false);  
        }      
    }
}
