﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneManagement : MonoBehaviour
{
    public GameObject enemy;
    public GameObject player;
    public GameObject camera;
    //script executed on button clicks of all menu buttons.
    //uses the sceneManagement class to load various scenes in project
    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Cursor.lockState = CursorLockMode.None;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    //executed on exiting of the pausemenu.
    //allows the player and camera to rotate and sets the gamestop variable in the enemy class to false to enable audio.
    public void PauseMenu()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        enemy.GetComponent<Enemy>().gameStop = false;
        player.GetComponent<PlayerMovement>().canPlayerMove = true;
        camera.GetComponent<CameraMovement>().canCameraRotate = true;
        gameObject.SetActive(false);
    }

    public void Controls()
    {
        SceneManager.LoadScene("Control");
    }
}
            

