﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    //creates a float for enemy view distance/range
    public float viewDistance = 50;
    //creates a float to time how long a player has been detected for
    public float escapeTime = 0;
    //creates fields for drag and drop of enemy animator in inspector. assigned as control
    public Animator control;   
    public GameObject player;
    public GameObject endLose;
    public GameObject endScreenCam;
    public GameObject playerAlert;
    //creates fields to be referenced and set later
    public EnemyIdle idleState;
    public EnemyPatrol patrolState;
    public EnemyChase chaseState;
    public EnemyStun stunState;
    public SmartPatrol patrolSmart;
    public EnemyAttack attackState;
    //creates bools to check if the enemy is stunned and if the game has been stopped (in the case of a menu being opened)
    public bool isStunned = false;
    public bool gameStop = false;
    //creates a private readonly instance of the finite state machine class, stateMachine cannot be assigned anything else except at this declaration
    private readonly FiniteStateMachine stateMachine = new FiniteStateMachine();
    //creates bools to check if the player is in range of the ai and if the alert property is enabled.
    public bool targetInRange = false;
    public bool alert = false;
    public bool playerVisible = false;
    //creates navmeshagent field that can only be read but not set outside of this class
    public NavMeshAgent Agent { get; private set; }
    //creates transform field for the Ai target, can only be read but not set outside of the class
    public Transform Target { get; private set; }   
    //calls the following code upon initialization before the start function
    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();       
    }
    private void Start()
    {
        //finds the gameobject with tag player
        Target = GameObject.FindWithTag("Player").transform;
        //sets the previously created fields to new instances of the classes that they are type of. takes the parameters enemy instance and behavior state
        idleState = new EnemyIdle(this, idleState);
        patrolState = new EnemyPatrol(this, patrolState);
        chaseState = new EnemyChase(this, chaseState);
        stunState = new EnemyStun(this, stunState);
        patrolSmart = new SmartPatrol(this, patrolSmart);
        attackState = new EnemyAttack(this, attackState);
        //sets the default state to idle
        stateMachine.SetState(idleState);      
    }
    private void Update()
    {
        PlayerTrack();
        stateMachine.StateUpdate();
        Stun();     
        //if enemy alerted 
        if (alert == true)
        {
            //if not stunned or not already chasing, sets state to chase and sets playerAlert to true to notify the player
            if (stateMachine.CurrentState != stunState && stateMachine.CurrentState != chaseState && stateMachine.CurrentState != attackState)
            {
                playerAlert.SetActive(true);
                stateMachine.SetState(chaseState);
            }
            //if stunned enter the stun state
            else if (isStunned == true)
            {
                stateMachine.SetState(stunState);
            }
            
        }

        //if games is paused stop any audio currently playing
        if (gameStop == true)
        {
            stateMachine.CurrentState.source.Stop();
        }
        //creates variable for the current enemy animator state info
        var currentAnim = control.GetCurrentAnimatorStateInfo(0);
        //if enemy playing attack animation, let it finish and then effectively end the game
        //sets timescale to 0
        //ulocks the cursor
        //stops audio
        //opens the end game screen ands ets the camera to endgame camera
        if (currentAnim.IsName("Attack") && currentAnim.normalizedTime >= 1)
        {
            Time.timeScale = 0;
            endLose.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            player.SetActive(false);
            stateMachine.CurrentState.source.Stop();
            endScreenCam.SetActive(true);
        }
    }
    private void PlayerTrack()
    {
        //if the enemy not alerted and the linecast hits a collider
        if (Physics.Linecast(transform.position, player.transform.position, out RaycastHit hit))
        {
            Debug.DrawLine(transform.position, player.transform.position, Color.blue);
            //if the collider hit is the player and they are within enemy range
            if (hit.collider.tag.Equals("Player") && Vector3.Distance(transform.position, player.transform.position) < viewDistance)
            {
                //start timing how long they are detected for and set target in range to true
                escapeTime += Time.deltaTime;
                targetInRange = true;
                if (alert == false)
                {
                    //if not already chasing the player and not stunned and not attacking set the state to chase
                    if (stateMachine.CurrentState != chaseState && stateMachine.CurrentState != stunState && stateMachine.CurrentState != attackState && endScreenCam.activeInHierarchy == false)
                    {
                        stateMachine.SetState(chaseState);
                    }
                }
                //if alert true set player visible to true
                else 
                {
                    playerVisible = true;
                }
            }
            //if there is an object blocking the Ai from the player or the player exits the viewdistance
            else if (hit.collider.tag.Equals("Player") == false && alert == false && stateMachine.CurrentState != attackState || Vector3.Distance(transform.position, player.transform.position) > viewDistance && alert == false && stateMachine.CurrentState != attackState)
            {
                //if the player has been detected but no longer is
                if (escapeTime > 0)
                {
                    targetInRange = false;
                    //creates a list of the patrol points
                    List<float> pointDis = new List<float>();
                    //iterates through the patrol point transform array and adds a float of the players distance from each point to the list just created
                    foreach (Transform point in patrolState.patrolPoints)
                    {
                        var distance = Vector3.Distance(player.transform.position, point.transform.position);
                        pointDis.Add(distance);
                    }
                    //converts the list to an array and checks the smallest distance float of the the list
                    patrolSmart.smartPatrolIndex = pointDis.IndexOf(Mathf.Min(pointDis.ToArray()));
                    //sets the escape time back to 0 for next detection
                    escapeTime = 0;
                    //sets the state to the smart patrol state
                    stateMachine.SetState(patrolSmart);
                }
            }
            //if alert is true but a wall is blocking the player from enemy enemy can't attack them
            else if (alert == true && hit.collider.tag.Equals("Player") == false)
            {
                playerVisible = false;
            }
        }
    }
    private void Stun()
    {
        //if the ai is stunned (set through the player interaction script)set the state to stunstate and start tje cooldown timer
        if (isStunned == true && stateMachine.CurrentState != stunState)
        {            
            stateMachine.SetState(stunState);
        }
    } 
    private void OnDrawGizmos()
    {
        //creates a sphere for visualisation
        Gizmos.DrawWireSphere(transform.position, viewDistance);
    }
    [System.Serializable]
    public class EnemyIdle : FiniteStateMachine.BehaviourState
    {
        //creates a public field to adjust the Ai idle times
        public Vector2 waitRange;
        //creates floats to check when the wait time has been reached
        private float time = 0;
        private float timer = -1;
        //constructor for the enemy idle class as it inherits from the behaviour state
        public EnemyIdle(Enemy i, EnemyIdle idle) : base(i)
        {
            //sets the waitrange to this instance of enemyidle
            //sets the source to the component audiosource and the clip to this classes instance of clip
            waitRange = idle.waitRange;
            source = instance.GetComponent<AudioSource>();
            audio = idle.audio;
        }
        public override void StateEnter()
        {
            //plays the audio clip
            source.PlayOneShot(audio);
            //sets the animation to idle
            control.SetBool("Idle", true);
            //sets timer to 0 and sets our wait time to a random float between the numbers entered in the inspector
            //sets the navmeshagent to stopped.
            timer = 0;
            time = Random.Range(waitRange.x, waitRange.y);
            instance.Agent.isStopped = false;
        }
        public override void StateUpdate()
        {
            //if the timer has been set to 0 through the state enter
            if (timer >= 0)
            {
                //timer starts counting until it reaches equal to the time that has been chosen through random range
                timer += Time.deltaTime;
                if (timer >= time)
                {
                    //sets the timer back and sets ai state to patrol
                    timer = -1;
                    instance.stateMachine.SetState(instance.patrolState);
                }
            }
        }
        public override void StateExit()
        {
            //upon exiting the state the animation is stopped
            control.SetBool("Idle", false);
        }
    }
    [System.Serializable]
    public class EnemyPatrol : FiniteStateMachine.BehaviourState
    {
        //creates float for the patrol speed of the ai
        public float patrolSpeed = 15;
        //creates a transform array 
        public Transform[] patrolPoints;
        //sets the current index int to -1 for use with the transform array
        private int currentIndex = -1;
        public EnemyPatrol(Enemy i, EnemyPatrol patrolState) : base(i)
        {
            //sets the ID to the patrol
            ID = EnemyStateID.patrol;
            //sets the patrol points to the patrolpoints of this instance
            //patrols speed is the patrol speed of this instance
            patrolPoints = patrolState.patrolPoints;
            patrolSpeed = patrolState.patrolSpeed;
            //sets the source to the component audiosource and the clip to this classes instance of clip
            source = instance.GetComponent<AudioSource>();
            audio = patrolState.audio;
        }
        public override void StateEnter()
        {
            //plays the audio once
            source.PlayOneShot(audio);
            //sets the aniamtion paramter patrolling to true
            control.SetBool("Patrolling", true);
            //stops the navmesh agent
            instance.Agent.isStopped = true;
            //runs the get next index method
            GetNextIndex();
        }
        public override void StateUpdate()
        {
            //creates a vector3 of the ais position in terms of x and z axis         
            Vector3 PLayerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
            //creates a vector3 of the current patrolpoints position in temrs of the x and z axis
            Vector3 TargetPos2D = new Vector3(patrolPoints[currentIndex].position.x, 0, patrolPoints[currentIndex].position.z);
            //gets the distance between these vector3
            float distance = Vector3.Distance(PLayerPos2D, TargetPos2D);
            //if the distance between the navmeshagent stopping distance is less than the distance variable sethe ai to idle state
            if (distance <= instance.Agent.stoppingDistance)
            {
                instance.stateMachine.SetState(instance.idleState);
            }            
            else
            {                
                Debug.DrawLine(instance.transform.position, patrolPoints[currentIndex].position, Color.red);
                //if the navmeshagent is stopped and the current state is no stunned then allow the navmesh to move
                if (instance.Agent.isStopped == true && !instance.stateMachine.CurrentState.Equals(instance.stunState))
                {
                    instance.Agent.isStopped = false;
                }
                //sets the navmesh agents destination to the currentinde position
                instance.Agent.SetDestination(patrolPoints[currentIndex].position);

            }
        }
        public override void StateExit()
        {
            //stops the audio and animation that are playing
            source.Stop();
            control.SetBool("Patrolling", false);
        }
        private void GetNextIndex()
        {
            //gets the next patrol point index and if the current index is outside the bounds of the array (greater than the length) sets the current index back to 0
            currentIndex++;
                if (currentIndex >= patrolPoints.Length)
                {
                    currentIndex = 0;
                }
        }
    }

    [System.Serializable]
    public class SmartPatrol : FiniteStateMachine.BehaviourState
    {
        //creates a transform array and a float for the index of the patrolpoint
        public Transform[] patrolPoints;
        public float smartPatrolIndex;
        public SmartPatrol(Enemy i, SmartPatrol smartPatrol) : base(i)
        {
            //sets the ID to the smart patrol
            ID = EnemyStateID.smartPatrol;
            //sets the patrol points array to equal the patrol state array
            patrolPoints = instance.patrolState.patrolPoints;
            //audio source is set to the audiosource component
            source = instance.GetComponent<AudioSource>();
            //audio clip is this instances clip
            audio = smartPatrol.audio;           
        }
        public override void StateEnter()
        {
            //plays the audio once
            source.PlayOneShot(audio);
            //sets the animation parameter to true
            control.SetBool("Patrolling", true);
            //allows the navmesh agent to move
            instance.Agent.isStopped = false;            
        }
        public override void StateUpdate()
        {
            //creates an int to store the result of the int.tryparse
            int result;
            //if we are able to parse float as an int
            if (int.TryParse(smartPatrolIndex.ToString(), out result))
            {
                //set the nevmechagnets destination to the patrol point index of the int we just parsed
                instance.Agent.SetDestination(patrolPoints[int.Parse(smartPatrolIndex.ToString())].position);
                //creates a vector 3 of the ais position on the x and z axis
                Vector3 PLayerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
                //creats a vecotr3 of the current patrolpoints posiion on the x and z axis
                Vector3 TargetPos2D = new Vector3(patrolPoints[int.Parse(smartPatrolIndex.ToString())].position.x, 0, patrolPoints[int.Parse(smartPatrolIndex.ToString())].position.z);
                //finds the distance between the two vector3
                float distance = Vector3.Distance(PLayerPos2D, TargetPos2D);
                //if the distance variable is less than the stopping distance then set the ai to the idle state
                if (distance <= instance.Agent.stoppingDistance)
                {
                    instance.stateMachine.SetState(instance.idleState);
                }
            }                   
        }

        public override void StateExit()
        {
            //stops any audio playing
            source.Stop();
            //turns off the aniamtion parameter
            control.SetBool("Patrolling", false);
        }
    }

    [System.Serializable]
    public class EnemyChase : FiniteStateMachine.BehaviourState
    {
        //creates a float for the chase speed of the ai
        public float chaseSpeed = 30;
        public EnemyChase(Enemy i, EnemyChase chaseState) : base (i)
        {
            //sets the ID to the chase 
            ID = EnemyStateID.chase;
            //chase speed is equal to the chase speed of this class instance
            chaseSpeed = chaseState.chaseSpeed;
            //source is equal to the audiosource component
            source = instance.GetComponent<AudioSource>();
            //audio clip is this instances audio
            audio = chaseState.audio;
        }

        public override void StateEnter()
        {
            //plays the audio once
            source.PlayOneShot(audio);
            //sets the aniamtion paramter to true
            control.SetBool("Chasing", true);
            //allows the navm,eshagent to move
            instance.Agent.isStopped = false;
            //sets the nevmeshagents speed to the chasespeed variable value
            instance.Agent.speed = chaseSpeed;
        }
        public override void StateUpdate()
        {
            //if the target is not null (gets rid of the null reference)
            if (instance.Target != null)
            {
                //if the targte is in range of the ai
                if (instance.targetInRange == true)
                {
                    //vector3 ignoring y axis 
                    //creates vector3's of both the ai position and the player position
                    Vector3 PLayerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
                    Vector3 TargetPos2D = new Vector3(instance.Target.position.x, 0, instance.Target.position.z);
                    //checks the distance between the two vector3's
                    float distance = Vector3.Distance(PLayerPos2D, TargetPos2D);
                    //if the distance is less than the nevmeshagents stopping idstance
                    if (distance <= instance.Agent.stoppingDistance && instance.isStunned == false)
                    {
                        if (instance.alert == false)
                        {
                            //stop the nevmeshagent
                            instance.Agent.isStopped = true;
                            //set the ai state to the attack state
                            if (instance.stateMachine.CurrentState != instance.attackState)
                            {
                                instance.stateMachine.SetState(instance.attackState);
                            }
                        }
                        //if enemyu is alerted and the player is visible to it then it can attack
                        else if (instance.alert == true && instance.GetComponent<Enemy>().playerVisible == true)
                        {
                            //stops the agent moving                            
                            if (instance.stateMachine.CurrentState != instance.attackState)
                            {
                                instance.stateMachine.SetState(instance.attackState);
                            }
                        }
                       
                    }
                    //if the distance is greater than the stopping distance
                    else
                    {
                        Debug.DrawLine(instance.transform.position, instance.Target.position, Color.red);
                        //if the navmesh agent is stopped let it move
                        if (instance.Agent.isStopped == true)
                        {
                            instance.Agent.isStopped = false;
                        }
                        //set the ai destination to the player position
                        instance.Agent.SetDestination(instance.Target.position);
                    }
                }
                // if the target is not in range and if the ai alert proeperty is false set the state back to patrol
                else if (instance.alert == false)
                {
                    instance.stateMachine.SetState(instance.patrolState);
                }             
            }
        }

        public override void StateExit()
        {
            //set the animatio parameter back to false
            control.SetBool("Chasing", false);
            source.Stop();
        }
    }

    [System.Serializable]
    public class EnemyStun : FiniteStateMachine.BehaviourState
    {
        //creates a stun time float in the inspector
        public float stunTime = 3;
        //creates a timer to time the stun
        private float timer = -1;
        public EnemyStun(Enemy i,EnemyStun stunState) : base(i)
        {
            //sets the ID to the  stun state
            ID = EnemyStateID.stun;
            //stun time is equal to this instance of stuntime
            stunTime = stunState.stunTime;
            //source equals the audiosource componenent
            source = instance.GetComponent<AudioSource>();
            //audio is this instances audio clip
            audio = stunState.audio;
        }
        public override void StateEnter()
        {
            //plays the audio once
            source.PlayOneShot(audio);
            //sets the timer to 0
            timer = 0;
            //allows the nevmeshagnet to move
            instance.Agent.isStopped = true;
            //enters stun aminator state
            control.SetBool("Stun", true);
        }
        public override void StateUpdate()
        {
            //if the timer has been set to 0 or is greater than 0
            if (timer >= 0)
            {
                //the timeris increased by delta time
                timer += Time.deltaTime;
                //if timer reaches equal to the stuntime
                if (timer >= stunTime)
                {
                    //reset the timer
                    timer = -1;
                    //if the ai is not alerted
                    if (instance.alert == false)
                    {
                        //if the target is in range set the state to the chase state else set to the patrol state
                        if (instance.targetInRange == true)
                        {
                            instance.stateMachine.SetState(instance.chaseState);
                        }
                        else
                        {
                            instance.stateMachine.SetState(instance.patrolState);
                        }                    
                    }
                    //if the ai is alerted then chase the player
                    else
                    {
                        instance.stateMachine.SetState(instance.chaseState);
                    }
                }
            }
        }

        public override void StateExit()
        {
            //set the stunned bool to false
            var self = instance.GetComponent<Enemy>();
            self.isStunned = false;
            control.SetBool("Stun", false);
        }
    }

    [System.Serializable]
    public class EnemyAttack : FiniteStateMachine.BehaviourState
    {
        public EnemyAttack(Enemy i, EnemyAttack attackState) : base(i)
        {
            //sets the ID to the attack
            ID= EnemyStateID.attack;
            //source equals the audio compononet
            source = instance.GetComponent<AudioSource>();
            //audio clip is this instances audio clip
            audio = attackState.audio;
        }

        public override void StateEnter()
        {
            //makes the ai look at the player
            instance.gameObject.transform.LookAt(instance.Target.position);
            //makes the  player look at the ai
            instance.Target.transform.LookAt(new Vector3(instance.transform.position.x, 7, instance.transform.position.z));
            //plays the audio
            source.PlayOneShot(audio);   
            //allows the navmeshagent to move
            instance.Agent.isStopped = true;
            //sets the animation paramter to true
            control.SetBool("Attack", true);
            //stops the player from moving
            instance.Target.GetComponent<PlayerMovement>().canPlayerMove = false;
            instance.targetInRange = false;
            //stops the player stunning
            instance.Target.GetComponentInChildren<PlayerInteraction>().stunCooldown = 0;
        }

        public override void StateUpdate()
        {
            
        }
    }
}

public class FiniteStateMachine
{
    //creates a field of behaviour state that can only be set from this class
    public BehaviourState CurrentState { get; private set; }
   //void to set the state
   //takes the paramter of type behaviour state
    public void SetState(BehaviourState newState)
    {
        Debug.Log("Entering new state - " + newState.GetType());
        //if there is a current state then execute the state exit function       
        if (CurrentState != null)
        {
            CurrentState.StateExit();            
        }
        //current state is equal to the parameter passed
        //runs the state enterfunction of the passed state
        CurrentState = newState;
        CurrentState.StateEnter();
    }

    public void StateUpdate()
    {
        //if there is a current state execute the state update function
        if (CurrentState != null)
        {
            CurrentState.StateUpdate();
        }
    }
    /// <summary>
    /// Template class for defining new enemy states
    /// </summary>
    public abstract class BehaviourState
    {
        //crates a animator, audiosource and audioclip field to be accessed and set by each class inheriting
        public Animator control;
        public AudioSource source;
        public AudioClip audio;
        //creates an enum to store all the states       
        public enum EnemyStateID { idle, chase, patrol, stun, smartPatrol, attack}
        //creates an fields only accessible through a class inheriting from this class (where the instance was created)
        protected Enemy instance;
        protected EnemyStateID ID;
        

        public BehaviourState(Enemy i)
        {
            //sets the instance to the paramater that is passed in behaviourstates enemy paramater
            instance = i;
            //control is equalt to the animator component of this gameobject
            control = instance.GetComponent<Animator>();                 
        }
        /// <summary>
        /// creates virtual voids to be overwrittten
        /// </summary>
        public virtual void StateEnter() { }

        public virtual void StateExit() { }

        public abstract void StateUpdate();
    }
}
